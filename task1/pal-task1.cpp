#include <iostream>
#include <cstdio>
#include "structs.h"

#define LOCAL_TEST

int main(void) {
	int a, b, c, nodesN, edgesN;
	CGraph graph;

#ifdef LOCAL_TEST
	freopen("tests/pub01.in", "r", stdin);
#endif

	scanf("%d%d", &nodesN, &edgesN);
	while (scanf("%d%d%d", &a, &b, &c) != EOF) graph.addEdge(a, b, c);

#ifdef LOCAL_TEST	
	graph.checkCounts(nodesN, edgesN);
#endif	

	std::cout << graph.search();

#ifdef LOCAL_TEST
	freopen("tests/pub01.out", "r", stdin);
	std::cin >> a;
	std::cout << " " << a << std::endl;
	fclose(stdin);	
#endif
    return 0;
}

