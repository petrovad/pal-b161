#include "structs.h"
#include <iostream>
#include <algorithm>

void CGraph::addEdge(const int a, const int b, const int c) {
	nodes[a].neighbours.insert(std::pair<int, TNode *>(c, &nodes[b]));
	nodes[a].id = a;
	nodes[b].id = b;
	if (c) dmged++;
}

void CGraph::checkCounts(unsigned int nodesCount, unsigned int edgesCount) {
	unsigned int edgesN = 0;
	for (auto it : nodes) edgesN += it.second.neighbours.size();
	if (edgesN != edgesCount) std::cerr << "Edges do not match!" << std::endl;
	if (nodes.size() != nodesCount) std::cerr << "Nodes do not match!" << std::endl;
}

void CGraph::strongconnect(TNode * node) {
	node->index = index;
	node->lowlink = index;
	index++;
	tarjanStack.push(node);
	node->onStack = true;

	for (auto & it : node->neighbours) {
		if (it.second->index == -1) {
			this->strongconnect(it.second);
			node->lowlink = std::min(node->lowlink, it.second->lowlink);
		}
		else if (it.second->onStack) node->lowlink = std::min(node->lowlink, it.second->lowlink);
	}

	if (node->lowlink == node->index) {
		std::set<TNode *> scc;
		TNode * topNode;
		do {
			topNode = tarjanStack.top();
			tarjanStack.pop();
			topNode->onStack = false;
			scc.insert(topNode);
		} while (topNode != node);
		components.insert(std::pair<int, Component>(sccN++, Component(scc)));
	}
}

void CGraph::listComponents(void) const {
	for (auto scc : components) std::cout << scc.first << ": " << scc.second << "\n";
	std::cout << std::endl;
}

void CGraph::removeComponents(void) {
	// create new graph without any edges
	for (auto & scc : components) {
		condensed[scc.first].id = scc.first;
		condensed[scc.first].value = scc.second.value;
	}

	for (auto & scc : components)
		for (auto & node : scc.second.nodes) node->sccId = scc.first;
	
	for (auto node : nodes) {
		for (auto neighbour : node.second.neighbours) {
			std::pair<int, TNode *> newNeighbour = std::pair<int, TNode *>(neighbour.first, &condensed[neighbour.second->sccId]);
			
			if (condensed[node.second.sccId].neighbours.find(newNeighbour) != condensed[node.second.sccId].neighbours.end()) continue;

			if (neighbour.first == 1) {
				std::set<std::pair<int, TNode *>>::iterator it = condensed[node.second.sccId].neighbours.find(std::pair<int, TNode *>(0, &condensed[neighbour.second->sccId]));
				if (it != condensed[node.second.sccId].neighbours.end()) condensed[node.second.sccId].neighbours.erase(it);
			}
			condensed[node.second.sccId].neighbours.insert(newNeighbour);
		}
	}

	// print condensed graph
	/*for (auto node : condensed) {
		std::cout << node.first << ", val: " << node.second.value << "\n";
		for (auto neighbour : node.second.neighbours) {
			std::cout << neighbour.first << " " << neighbour.second->id << "\n";
		}
		std::cout << std::endl;
	}*/
}

int walk(TNode * node) {
	int path = 0;
	for (auto neighbour : node->neighbours) {
		int currPath = neighbour.first;
		
		if (neighbour.second->state == NODE_UNVISITED) currPath += walk(neighbour.second);
		else currPath += neighbour.second->value;
		
		if (currPath > path) path = currPath;
	}

	node->value += path;
	node->state = NODE_VISITED;
	return node->value;
}

int CGraph::search(void) {
	for (auto & it : nodes) {
		if (it.second.index == -1) this->strongconnect(&it.second);
	}

	//this->listComponents();
	this->removeComponents();

	for (auto it : condensed) {
		if (it.second.state == NODE_UNVISITED) walk(&it.second);
		if (it.second.value > walked) walked = it.second.value;
		if (walked == dmged) break;
	}
	return walked;
}

bool operator < (const std::pair<int, TNode *> & a, const std::pair<int, TNode *> & b) {
	if (a.second->id == b.second->id) return a.first < b.first;
	else return a.second->id < b.second->id;
}

std::ostream & operator<<(std::ostream & out, const Component & scc) {
	for (auto node : scc.nodes) out << node->id << " ";
	return out << "\tval:" << scc.value;
}

Component::Component(std::set<TNode*>& scc)
	: value(0),
	  nodes(scc) {
	for (auto node : nodes) {
		for (auto neighbour : node->neighbours) {
			if (this->hasNode(neighbour.second->id)) value += neighbour.first;
		}
	}
}

bool Component::hasNode(const int id) const {
	for (auto node : nodes) if (node->id == id) return true;
	return false;
}

std::list<std::pair<int, TNode*>> Component::getOutgoing(void) const {
	std::list<std::pair<int, TNode *>> outgoing;
	for (auto node : nodes) {
		for (auto neighbour : node->neighbours) {
			if (!this->hasNode(neighbour.second->id)) outgoing.push_back(std::pair<int, TNode *>(neighbour.first, neighbour.second));
		}
	}

	return outgoing;
}
