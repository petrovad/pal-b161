#ifndef _PALTSK1_STRUCTS_H
#define _PALTSK1_STRUCTS_H

#include <map>
#include <list>
#include <stack>
#include <set>

enum nodeState {
	NODE_OPEN, NODE_VISITED, NODE_UNVISITED
};

struct TNode {
	TNode(void) : state(NODE_UNVISITED), value(0), index(-1), onStack(false) { }

	nodeState state;
	int id, value, index, lowlink, sccId;
	bool onStack;
	std::set<std::pair<int, TNode *>> neighbours;

	bool operator < (const TNode & a) const { return id < a.id; }
};

bool operator < (const std::pair<int, TNode *> & a, const std::pair<int, TNode *> & b);

class Component {
public:
	Component(std::set<TNode *> & scc);

	bool hasNode(const int id) const;
	std::list<std::pair<int, TNode *>> getOutgoing(void) const;

	friend std::ostream & operator << (std::ostream & out, const Component & scc);

	int value;

	std::set<TNode *> nodes;
};

class CGraph {
public:
	CGraph(void) : dmged(0), walked(0), index(0), sccN(0) { }
	void addEdge(const int a, const int b, const int c);
	void checkCounts(unsigned int nodesCount, unsigned int edgesCount);
	int search(void);

private:
	void strongconnect(TNode * node);
	void listComponents(void) const;
	void removeComponents(void);

	std::map<int, TNode> nodes, condensed;
	int dmged, walked, index, sccN;
	std::stack<TNode *> tarjanStack;
	std::map<int, Component> components;
};

#endif // !_PALTSK1_STRUCTS_H