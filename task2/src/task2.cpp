#include <iostream>
#include <cstdio>
#include "structs.h"

#define LOCAL_TEST

int main(void) {
	int a, b, nodesN, edgesN, x, y;
	CGraph graph;

#ifdef LOCAL_TEST
	freopen("datapub/pub05.in", "r", stdin);
#endif

	scanf("%d%d", &nodesN, &edgesN);
	while (scanf("%d%d", &a, &b) != EOF) graph.addEdge(a, b);

#ifdef LOCAL_TEST	
	graph.checkCounts(nodesN, edgesN);
#endif	

	graph.certificates(x, y);
	std::cout << x << " " << y << std::endl;

#ifdef LOCAL_TEST
	freopen("datapub/pub05.out", "r", stdin);
	scanf("%d%d", &a, &b);
	std::cout << a << " " << b << std::endl;
	fclose(stdin);
#endif
	return 0;
}
