#include "structs.h"
#include <iostream>
#include <vector>
#include <algorithm>

void CGraph::addEdge(const int a, const int b) {
	nodes[a].neighbours.push_back(&nodes[b]);
	nodes[b].neighbours.push_back(&nodes[a]);
	nodes[a].id = a;
	nodes[b].id = b;
}

void CGraph::checkCounts(unsigned int nodesCount, unsigned int edgesCount) {
	unsigned int edgesN = 0;
	for (auto it : nodes) edgesN += it.second.neighbours.size();
	if (edgesN != 2 * edgesCount) std::cerr << "Edges do not match!" << std::endl;
	if (nodes.size() != nodesCount) std::cerr << "Nodes do not match!" << std::endl;
}

void CGraph::certificates(int & x, int & y) {
	int erased = 1;

	while (erased != 0) {
		erased = 0;
		for (auto node = nodes.begin(); node != nodes.end(); ++node) {
			if (node->second.neighbours.size() > 1) {	
				std::vector<std::string> labels;
				labels.push_back(node->second.label.substr(1, node->second.label.size() - 2));

				for (auto neighbour = node->second.neighbours.begin(); neighbour != node->second.neighbours.end(); ) {
					if ((*neighbour)->neighbours.size() == 1) {
						labels.push_back((*neighbour)->label);
						//nodes.erase((*neighbour)->id);
						(*neighbour)->toErase = true;
						neighbour = node->second.neighbours.erase(neighbour);
						erased++;
					}
					else ++neighbour;
				}

				sort(labels.begin(), labels.end());

				node->second.label = '0';
				for (auto label : labels) node->second.label += label;
				node->second.label += '1';
			}
		}
		for (auto node = nodes.begin(); node != nodes.end(); ) {
			if (node->second.toErase) node = nodes.erase(node);
			else ++node;
		}
	}

	for (auto node : nodes) certs[node.second.label] = 0;
	for (auto node : nodes) certs[node.second.label]++;

	if (certs.size() > 2 || certs.size() < 1) {
		std::cerr << "Error: Wrong number of certificates: " << certs.size() << "!" << std::endl;
		//for (auto cert = certs.cbegin(); cert != certs.cend(); ++cert) std::cerr << cert->first << " " << cert->second << std::endl;
	}
	else if (certs.size() == 1) {
		x = 0;
		y = certs.cbegin()->second;
	}
	else {
		x = certs.cbegin()->second;
		y = (++certs.cbegin())->second;

		if (x > y) {
			int tmp = x;
			x = y;
			y = tmp;
		}
	}
}
