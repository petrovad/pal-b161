#ifndef _PALTSK2_STRUCTS_H
#define _PALTSK2_STRUCTS_H

#include <map>
#include <string>
#include <list>

struct TNode {
	TNode(void) : label("01"), toErase(false) { }

	std::string label;
	int id;
	bool toErase;

	std::list<TNode *> neighbours;
};

class CGraph {
public:
	CGraph(void) { }
	void addEdge(const int a, const int b);
	void checkCounts(unsigned int nodesCount, unsigned int edgesCount);
	void certificates(int & x, int & y);

private:
	std::map<int, TNode> nodes;
	std::map<std::string, int> certs;
};

#endif // !_PALTSK2_STRUCTS_H