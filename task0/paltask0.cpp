#include <iostream>
#include <cstdio>
#include <cmath>
 
float euclidean(int x1, int y1, int x2, int y2) {
	return sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));
}

int main() {
	int n, x0, y0, x1, y1, x2, y2;
	double len = 0;
	
	scanf("%d", &n);
	for (int i = 0; i < n; i++) {
		scanf("%d%d", &x2, &y2);

		if (i > 0) len += euclidean(x1, y1, x2, y2);
		else {
			x0 = x2;
			y0 = y2;
		}
		x1 = x2;
		y1 = y2;
	}

	len += euclidean(x2, y2, x0, y0);
	printf("%.0f", ceil(5 * len));

    return 0;
}

